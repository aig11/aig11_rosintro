rosservice call /reset
rosservice call turtle1/set_pen 10 10 250 5 'off' 
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[0.0, 0.0, 0.0]' '[0.0, 0.0, 1.3]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[3.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[0.0, 0.0, 0.0]' '[0.0, 0.0, -2.6]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[3.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[-1.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[0.0, 0.0, 0.0]' '[0.0, 0.0, -1.74]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[1.1, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rosservice call /spawn 8 8.5 0 "turtle2"
rosservice call turtle2/set_pen 250 10 10 5 'off'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist '[0.0, 0.0, 0.0]' '[0.0, 0.0, -1.57]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist '[2.8, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist '[0.0, 0.0, 0.0]' '[0.0, 0.0, 1.57]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist '[1, 0.0, 0.0]' '[0.0, 0.0, 0.0]'